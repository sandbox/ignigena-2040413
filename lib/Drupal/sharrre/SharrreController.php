<?php

/**
 * @file
 * Contains \Drupal\sharrre/SharrreController
 */

namespace Drupal\sharrre;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Component\Serialization\Json;

class SharrreController {

  public $availableWidgets;
  public $sharrreURL;

  /**
   * Constructs a SharrreController object.
   *
   * Builds the complete list of available widgets.
   * @see hook_sharrre_widgets()
   */
  public function __construct() {
    $this->availableWidgets = \Drupal::moduleHandler()->invokeAll('sharrre_widgets');
  }

  /**
   * Validate whether the request is for a valid widget.
   *
   * @param string $widget
   *   The widget type as defined in $this->availableWidgets.
   *
   * @return BOOL
   *   If valid this will pass, otherwise it fails.
   */
  public function validateWidget($widget) {
    // By setting widget as 'all' this will pass automatically.
    if (!$widget == 'all' && !in_array($widget, $this->availableWidgets)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Gets the full URL from a node and stores it.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   A fully loaded node reference.
   */
  public function setURLFromNode($node) {
    global $base_url;
    $this->sharrreURL = $base_url . '/' . node_uri($node)->toString();
  }

  /**
   * Display the share counts associated with a node.
   *
   * @todo Extend to other entities/front page/arbitrary URL.
   *
   * @param string $widget
   *   The widget type as defined in $this->availableWidgets.
   * @param integer $id
   *   The node ID to fetch share counts for.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The share counts as json.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   Thrown when the named widget or node ID isn't valid.
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Thrown when the user does not have access to view the node.
   */
  public function buttonCount($widget, $id) {
    // Validate the widget is valid, and the node ID is an actual node.
    if (!$this->validateWidget($widget) || !is_numeric($id) || !$node = node_load($id)) {
      throw new NotFoundHttpException();
    }

    // Validate that the user has view access to the node.
    if (!$node->access('view')) {
      throw new AccessDeniedHttpException();
    }

    // Get and store the full URL from the loaded node.
    $this->setURLFromNode($node);

    // Start building the response.
    $response = array(
      'url' => 'http://sevenly.org', //$this->sharrreURL,
      'shares' => 0,
    );

    // By passing 'all' as our widget we loop through every available widget.
    if ($widget == 'all') {
      $total_share_count = 0;

      // Global results are tallied with each widget stored individually.
      foreach ($this->availableWidgets as $widget => $settings) {
        // Check to see if the widget is enabled in the settings.
        // Use the default as defined by the widget if no settings present.
        $config = \Drupal::config('sharrre.widgets');
        $widget_enabled = $config->get('enabled.' . $widget);
        if (!isset($widget_enabled)) $widget_enabled = isset($settings['enabled']);

        if ($widget_enabled) {
          $share_count = $this->sharrreGetCount($response['url'], $settings);
          $response[$widget] = array('shares' => $share_count);
          $total_share_count += $share_count;
        }
      }

      $response['shares'] = $total_share_count;
    }
    else {
      $response['shares'] = $this->sharrreGetCount($response['url'], $this->availableWidgets[$widget]);
    }

    // Return a JSON formatted array.
    return new JsonResponse($response);
  }

  /**
   * Fetch the share count for a URL.
   *
   * @param string $url
   *   The URL to fetch a share count for.
   * @param array $settings
   *   The settings defined by the widget implementation.
   *   @see hook_sharrre_widget()
   *
   * @return integer
   *   The total number of shares.
   */
  public function sharrreGetCount($url, $settings) {
    // If there is no return value in settings, stop now.
    if (!isset($settings['return'])) {
      return 0;
    }

    // Replace the {{url}} token in the API url.
    $apiurl = str_replace('{{url}}', $url, $settings['url']);

    // Allow for API calls that require POST such as Google Plus.
    if (isset($settings['post'])) {
      // If the POST content contains the URL, swap it out here.
      if (isset($settings['post_url'])) {
        $this->arrayReplace($settings['post'], $settings['post_url'], $url);
      }

      // Build the POST request.
      $context  = stream_context_create(array('http' => array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/json',
        'content' => Json::encode($settings['post']),
      )));
      $rawdata = file_get_contents($apiurl, false, $context);
    }
    // Otherwise, we send a standard GET request to the URL.
    else {
      $rawdata = file_get_contents($apiurl);
    }

    // Some voodoo because Pinterest doesn't return properly structured JSON.
    // @todo This can be removed once Pinterest fixes their shit.
    $rawdata = substr($rawdata, strpos($rawdata,'{'));
    $rawdata = substr($rawdata, 0, strrpos($rawdata,'}')+1);

    // Decode the JSON into an array so we can grab the share count.
    $return = Json::decode($rawdata);

    // Check the widget settings for the array key containing the share count.
    if (!is_array($settings['return'])) {
      return $return[$settings['return']];
    }
    // Allow for keys that are nested in the array
    else {
      foreach ($settings['return'] as $arrayitem) {
        $return = $return[$arrayitem];
      }
      return $return;
    }
  }

  /**
   * Helper function to replace an item in a multi-dimensional array.
   *
   * @param array $array
   *   The array to run the replace on.
   * @param string $keys
   *   A string of keys separated by '/' representing each key in the array.
   * @param string $value
   *   The replacement value once this key is found.
   */
  public function arrayReplace(array &$array, $keys, $value) {
    $val = &$array;
    foreach (explode('/', $keys) as $part) {
      if (!isset($val[$part])) {
        $val[$part] = array();
      }
      $val = &$val[$part];
    }
    $val = $value;
  }
}
