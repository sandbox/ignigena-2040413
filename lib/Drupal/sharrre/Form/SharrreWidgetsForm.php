<?php

/**
 * @file
 * Contains \Drupal\sharrre\Form\SharrreWidgetsForm.
 */

namespace Drupal\sharrre\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\sharrre\SharrreController;
use Drupal\Core\Form\ConfigFormBase;

class SharrreWidgetsForm extends ConfigFormBase {

  /**
   * Stores the configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  protected $widgets;

  public function __construct(ConfigFactory $config_factory) {
    $this->configFactory = $config_factory;
    $this->widgets = new SharrreController();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'system_site_information_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $form['widgets'] = array(
      '#type' => 'details',
      '#title' => t('Enabled Widgets'),
    );

    $config = $this->configFactory->get('sharrre.widgets');

    foreach ($this->widgets->availableWidgets as $widget => $settings) {
      $widget_enabled = $config->get('enabled.' . $widget);
      $form['widgets']['enabled_' . $widget] = array(
        '#type' => 'checkbox',
        '#title' => t($settings['title']),
        '#default_value' => isset($widget_enabled) ? $widget_enabled : isset($settings['enabled']),
      );
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    $enabled = array();

    foreach ($this->widgets->availableWidgets as $widget => $settings) {
      $enabled[$widget] = $form_state['values']['enabled_' . $widget];
    }

    $this->configFactory->get('sharrre.widgets')
      ->set('enabled', $enabled)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
